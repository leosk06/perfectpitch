#include "jniwrapper.h"

#include <assert.h>
#include <android/log.h>
#include "pffft.h"

#define AMPLITUDE_THRESHOLD 500     // any buffer with less amplitud will be treated as silence
int bufferSizeInSampleUnits;        // needs to be a power of 2
int sampleRate;                     // defined at the Java code
float *X, *Y, *Z;                   // buffers for PFFFT
PFFFT_Setup *pfftHandle;            // PFFFT library handle

// Cached Java classes and methods
jclass toastClass;
jmethodID toastMakeText, toastShow;
int toastLong;

// Forward declarations
float getMainFrequency(float *);
void toast(const char *, JNIEnv *, jobject);

// Library init
JNIEXPORT void JNICALL Java_com_example_perfectpitch_MainActivity_initLibrary(JNIEnv *env, jobject thiz, jint samplesPerBuffer) {
    bufferSizeInSampleUnits = samplesPerBuffer;

    __android_log_print(ANDROID_LOG_VERBOSE, "PFFFT", "Buffer size in Samples = %d", bufferSizeInSampleUnits);

    pfftHandle = pffft_new_setup(bufferSizeInSampleUnits, PFFFT_REAL);

    if (pfftHandle) {
        // Allocate buffers
        int Nbytes = bufferSizeInSampleUnits * sizeof(float);
        X = pffft_aligned_malloc(Nbytes);
        Y = pffft_aligned_malloc(Nbytes);
        Z = pffft_aligned_malloc(Nbytes);

        // get sample frequency
        jclass clazz = (*env)->GetObjectClass(env, thiz);
        jfieldID fIdSampleRate = (*env)->GetFieldID(env, clazz, "sampleRate", "I");
        sampleRate = (*env)->GetIntField(env, thiz, fIdSampleRate);
        __android_log_print(ANDROID_LOG_VERBOSE, "PFFFT", "PFFFT Sample rate (as per init) = %d", sampleRate);


        // cache methods to use android.widget.Toast
        toastClass = (*env)->FindClass(env, "android/widget/Toast");
        toastClass = (*env)->NewGlobalRef(env, toastClass);
        toastMakeText = (*env)->GetStaticMethodID(env, toastClass, "makeText", "(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;");
        toastShow = (*env)->GetMethodID(env, toastClass, "show", "()V");
        jfieldID toastLongID = (*env)->GetStaticFieldID(env, toastClass, "LENGTH_LONG", "I");
        toastLong = (*env)->GetStaticIntField(env, toastClass, toastLongID);

        toast("Initializing library...", env, thiz);
    } else {
        __android_log_print(ANDROID_LOG_VERBOSE, "PFFFT", "PFFFT Init failed");
    }
}

// Library close
JNIEXPORT void JNICALL Java_com_example_perfectpitch_MainActivity_destroyLibrary(JNIEnv *env, jobject thiz) {
    if (pfftHandle) {
        // Release buffers
        pffft_aligned_free(X);
        pffft_aligned_free(Y);
        pffft_aligned_free(Z);

        toast("Closing library", env, thiz);

        // Release global reference
        (*env)->DeleteGlobalRef(env, toastClass);

        pffft_destroy_setup(pfftHandle);
    }
}

// Get main frequency TODO: need to filter this
jint Java_com_example_perfectpitch_MainActivity_getFrequency(JNIEnv *env, jobject thiz, jshortArray samples) {
    int i, result;
    jboolean *isCopy;

    // length should equal bufferSizeInSampleUnits
    //jsize length = (*env)->GetArrayLength(env, samples);
    //assert(length == bufferSizeInSampleUnits);

    jshort *cSamples = (*env)->GetShortArrayElements(env, samples, isCopy);

    // Convert short array to the float array needed by PFFFT, and check whether the sound is too quiet
    int max, min;
    max = min  = 0;
    for (i = 0; i < bufferSizeInSampleUnits; i++) {
        X[i] = (float) cSamples[i];
        if (X[i] > max) {
            max = X[i];
        }

        if (X[i] < min) {
            min = X[i];
        }
    }

    if (max - min < AMPLITUDE_THRESHOLD) {
        return 0;
    }

    pffft_transform_ordered(pfftHandle, X, Z, Y, PFFFT_FORWARD);
    result = getMainFrequency(Z);

    (*env)->ReleaseShortArrayElements(env, samples, cSamples, JNI_ABORT);

    return result;
}

// Gets the main frequency component out of a Fourier transform, which is just the maximum frequency component
float getMainFrequency(float *Y) {
	int freq;
	float maxFreq = 0;
	float max = 0;

	for(freq=0; freq<bufferSizeInSampleUnits; freq++) {
		if(Y[freq] > max) {
			max = Y[freq];
			maxFreq = freq ;
		}
	}

	return sampleRate * maxFreq / bufferSizeInSampleUnits / 2;   //TODO: forgot why we had a 2 here

}

void toast(const char *text, JNIEnv *env, jobject context) {
    jstring jText = (*env)->NewStringUTF(env, text);
    jobject toastObj = (*env)->CallStaticObjectMethod(env, toastClass, toastMakeText, context, jText, toastLong);

    (*env)->CallVoidMethod(env, toastObj, toastShow);

    // Free up resources
    (*env)->DeleteLocalRef(env, jText);
}


