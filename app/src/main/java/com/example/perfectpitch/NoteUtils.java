package com.example.perfectpitch;

public class NoteUtils {

    private static float[] noteFrequencies = {
            //  Do    Do#     Re      Re#     Mi      Fa      Fa#     Sol     Sol#    La      La#     Si
            16.35f, 17.32f, 18.35f, 19.45f, 20.60f, 21.83f, 23.12f, 24.50f, 25.96f, 27.50f, 29.14f, 30.87f,
            32.70f, 34.65f, 36.71f, 38.89f, 41.20f, 43.65f, 46.25f, 49.00f, 51.91f, 55.00f, 58.27f, 61.74f,
            65.41f, 69.30f, 73.42f, 77.78f, 82.41f, 87.31f, 92.50f, 98.00f, 103.8f, 110.0f, 116.5f, 123.5f,
            130.8f, 138.6f, 146.8f, 155.6f, 164.8f, 174.6f, 185.0f, 196.0f, 207.7f, 220.0f, 233.1f, 246.9f,
            261.6f, 277.2f, 293.7f, 311.1f, 329.6f, 349.2f, 370.0f, 392.0f, 415.3f, 440.0f, 466.2f, 493.9f,
            523.3f, 554.4f, 587.3f, 622.3f, 659.3f, 698.5f, 740.0f, 784.0f, 830.6f, 880.0f, 932.3f, 987.8f,
            1047f, 1109f, 1175f, 1245f, 1319f, 1397f, 1480f, 1568f, 1661f, 1760f, 1865f, 1976f,
            2093f, 2217f, 2349f, 2489f, 2637f, 2794f, 2960f, 3136f, 3322f, 3520f, 3729f, 3951f,
            4186f, 4435f, 4699f, 4978f, 5274f, 5588f, 5920f, 6272f, 6645f, 7040f, 7459f, 7902f
    };

    private static String[] noteNames = new String[]{
            "Do", "Do#", "Re", "Re#", "Mi", "Fa", "Fa#", "Sol", "Sol#", "La", "La#", "Si"
    };

    public static String noteFromFrequency(int freq) {

        // Binary search
        int bottom = 0;
        int top = noteFrequencies.length;
        int index;
        while (top - bottom > 1) {
            index = (bottom + top) / 2;
            if (freq < noteFrequencies[index]) {
                top = index;
            } else {
                bottom = index;
            }
        }

        // See which of top and bottom we are closer to
        if (Math.abs(noteFrequencies[top] - freq)<Math.abs(noteFrequencies[bottom] - freq)) {
            index = top;
        } else {
            index = bottom;
        }

        // Are we close enough?
        if (Math.abs(noteFrequencies[index] - freq) < freq * 0.1) {
            return String.format("%s-%d (%dHz)", noteNames[index % noteNames.length], 1 + (int)Math.floor(index / noteNames.length), freq);
        }

        // There is sound but we hit no note
        return null;
    }
}
