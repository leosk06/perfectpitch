package com.example.perfectpitch;

import com.example.leo.mictest.R;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity {

    // C functions
    private native void initLibrary(int sampleRate);
    private native void destroyLibrary();
    private native int getFrequency(short[] samples);

    static {
        System.loadLibrary("pffft-test");
    }

    // Java
    public static final String TAG = "NDK_DEMO";
    private int sampleRate = 8000;

    private AudioRecord mRecorder;

    private static int mBufferSizeInBytes;
    private static int mBufferSizeInSampleUnits;

    private boolean mIsCapturing = false;

    private TextView mTxtNote;
    private Button mBtnStart;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Minimum buffer size
        mBufferSizeInBytes = AudioRecord.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

        // Next power of 2
        mBufferSizeInBytes = (int)Math.pow(2, Math.ceil(Math.log(mBufferSizeInBytes) / Math.log(2)))  * 8;
        mBufferSizeInSampleUnits = mBufferSizeInBytes / 2;  // sizeof(short)

        mBtnStart = (Button)findViewById(R.id.btn_start);
        mTxtNote = (TextView)findViewById(R.id.txt_note);

        mBtnStart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mIsCapturing = !mIsCapturing;
                if (mIsCapturing) {
                    mBtnStart.setText(R.string.stop);
                    startCapturing();
                } else {
                    mBtnStart.setText(R.string.start);
                    v.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mTxtNote.setText(R.string.no_note);
                        }
                    }, 500);
                }
            }
        });

    }

    public void startCapturing() {
        mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRate,
                                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                                    AudioFormat.ENCODING_PCM_16BIT, mBufferSizeInBytes);
        mRecorder.setPositionNotificationPeriod(mBufferSizeInSampleUnits);

        final short[] audioData = new short[mBufferSizeInSampleUnits];
        mRecorder.setRecordPositionUpdateListener(
                new AudioRecord.OnRecordPositionUpdateListener() {
                    @Override
                    public void onPeriodicNotification(AudioRecord recorder) {
                        mTxtNote.setText("" + getNoteAsText(audioData));
                    }

                    @Override
                    public void onMarkerReached(AudioRecord recorder) {
                    }
                }
        );

        mRecorder.startRecording();

        new Thread() {
            @Override
            public void run() {

                Log.d(TAG, "start recording, mBufferSizeInBytes: " + mBufferSizeInBytes);

                while (mIsCapturing) {
                    mRecorder.read(audioData, 0, mBufferSizeInSampleUnits); // We need shorts
                }
                Log.d(TAG, "stopped recording");
                mRecorder.release();
            }
        }.start();
    }

    private String getNoteAsText(short[] samples) {
        int freq = getFrequency(samples);

        if (freq == 0) {
            return getString(R.string.silence);
        }

        String note =  NoteUtils.noteFromFrequency(freq);
        if (note != null) {
            return note;
        } else {
            return getString(R.string.not_tuned);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        initLibrary(mBufferSizeInSampleUnits);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Set this in case we need to stop the listening thread
        mIsCapturing = false;

        destroyLibrary();
    }

}